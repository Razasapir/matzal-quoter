import requests
import random
from bs4 import BeautifulSoup
from tqdm import tqdm

RANDOM_PAGE = "https://he.wikiquote.org/wiki/%D7%9E%D7%99%D7%95%D7%97%D7%93:%D7%90%D7%A7%D7%A8%D7%90%D7%99"


def get_page():
    r = requests.get(RANDOM_PAGE)
    soup = BeautifulSoup(r.content, 'html5lib')
    return soup


def get_quote():
    soup = get_page()
    title = soup.find('h1').text
    table_of_contents = soup.find('div', attrs={'class': 'toctitle', 'lang': 'he', 'dir': 'rtl'})
    quote_list = soup.find('ul')
    if table_of_contents is not None:
        quote_list = soup.find_all('ul')[1]
    random_qoute = random.randrange(0, len(quote_list), 2)
    quote = quote_list.contents[random_qoute]
    try:
        return True, quote.text + " {" + title + "}."
    except Exception as e:
        return False, ""


def get_quotes(num=10):
    quotes = []
    pbar = tqdm(total=num)
    while len(quotes) < num:
        is_success, quote = get_quote()
        if is_success:
            quotes.append(quote)
            pbar.update(1)
    pbar.close()
    return quotes


def main():
    qs = get_quotes()
    for q in qs:
        print(q)


if __name__ == '__main__':
    main()

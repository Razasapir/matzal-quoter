import os
import re
import sys
import shutil
import lxml.etree as etree
import quotes

DOCUMENT = r"\word\document.xml"
ZIP_EXT = 'zip'
DOCX_EXT = 'docx'


def get_args():
    """
    Reads the parameters given in the cmd and handles exceptions.
    :return: Tuple: (True/False if parameters are fine, string path of maze.txt).
    """
    if not len(sys.argv) == 2:
        print("Wrong number of params, please enter the path of the matzal docx file.")
        return False, ""
    matzal_path = sys.argv[1]
    if not os.path.isfile(matzal_path):
        print("File path not found, please enter the path of the matzal docx file.")
        return False, ""
    if not os.path.basename(matzal_path)[-4:] == DOCX_EXT:
        print("Not a 'docx. file, please enter the path of the matzal docx file.")
        return False, ""
    return True, matzal_path


def change_ext(renamee, new_ext):
    """
    Change the extention of the file in order to convert file types.
    :param renamee: path of the file that is renamed.
    :param new_ext: the new file extension wanted.
    :return:
    """
    pre, ext = os.path.splitext(renamee)
    new_name = pre + "." + new_ext
    os.rename(renamee, new_name)
    return new_name


def dir_with_file_name(file_path_to_dir):
    """
    Create a directory with the same name of a file in the same path.
    :param file_path_to_dir:
    :return:
    """
    origin_dir = os.path.dirname(file_path_to_dir)
    new_dir_name, _ = os.path.splitext(os.path.basename(file_path_to_dir))
    new_dir = os.path.join(origin_dir, new_dir_name)
    os.mkdir(new_dir)
    return new_dir


def unzip(file_to_unzip, target_dir):
    """
    Unzips the zip file to the target dir
    :param file_to_unzip:
    :param target_dir:
    :return:
    """
    shutil.unpack_archive(file_to_unzip, target_dir, ZIP_EXT)


def zip_dir(dir_to_zip, zip_file):
    """
    Zips the directory to the zip file.
    :param dir_to_zip:
    :param zip_file:
    :return:
    """
    shutil.make_archive(zip_file, ZIP_EXT, dir_to_zip)
    return zip_file + "." + ZIP_EXT


def xml_to_string(xml_file_path):
    """
    Retrieve the content of an XML file as a string
    :param xml_file_path:
    :return:
    """
    tree = etree.parse(xml_file_path)  # or xml.dom.minidom.parseString(xml_string)
    return etree.tostring(tree, encoding='utf-8').decode()


def insert_in_position(main_string, index, to_insert):
    """
    Inserts the a string inside another string as a position index.
    :param main_string:
    :param index:
    :param to_insert:
    :return:
    """
    return main_string[:index] + " " + to_insert + main_string[index:]


def create_matzal(matzal_path):
    matzal_zip = change_ext(matzal_path, ZIP_EXT)  # Converts docx to zip.
    matzal_dir = dir_with_file_name(matzal_path)  # Create a directory to contain the unzipped files.
    unzip(matzal_zip, matzal_dir)
    change_ext(matzal_zip, DOCX_EXT)  # Reverts origin to docx

    xml_file_path = matzal_dir + DOCUMENT  # Path of the XML document data
    docx_content = xml_to_string(xml_file_path)
    places_to_insert = [m.end() for m in re.finditer("משפט:", docx_content)]  # Finds all occurrences of "משפט:"
    quotes_to_insert = quotes.get_quotes(len(places_to_insert))  # Retrieves random quotes from Wikiquote

    # Insert Quotes
    for i, index in enumerate(reversed(places_to_insert)):
        docx_content = insert_in_position(docx_content, index, quotes_to_insert[i])

    # Saves XML file
    with open(xml_file_path, 'wb') as xml_file:
        xml_file.write(docx_content.encode())

    # Zip data
    zipped_folder = zip_dir(matzal_dir, matzal_dir + "_finished")
    shutil.rmtree(matzal_dir)  # Deletes remains
    matzal_docx = change_ext(zipped_folder, DOCX_EXT)  # Converts final Matzal to docx
    return matzal_docx


if __name__ == '__main__':
    status, path = get_args()
    if status:
        create_matzal(path)

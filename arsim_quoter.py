import random
import requests
from bs4 import BeautifulSoup
from tqdm import tqdm
from urllib.parse import urljoin

SHIRONET = 'https://shironet.mako.co.il'
ARTIST_SONGS_HREF = '/artist?type=works&class=4&sort=popular&lang=1'

ARTIST_SONGS_CLASS = 'artist_player_songlist'
SONG_NAME_CLASS = 'artist_song_name_txt'
SONGS_ARTIST_CLASS = 'artist_singer_title'
SONG_LYRICS_CLASS = 'artist_lyrics_text'

ZOHAR_HAMELECH_ID = 373


def load_soup(r):
    soup = BeautifulSoup(r.content, 'html.parser')
    return soup


def load_artist_songs_page(artist_id=ZOHAR_HAMELECH_ID):
    url = urljoin(SHIRONET, ARTIST_SONGS_HREF)
    r = requests.get(url, params={'prfid': artist_id})
    return load_soup(r)


def load_song_page(href):
    url = urljoin(SHIRONET, href)
    r = requests.get(url)
    return load_soup(r)


def get_songs_list(artist_id=ZOHAR_HAMELECH_ID):
    soup = load_artist_songs_page(artist_id)
    songs_list = soup.findAll('a', {'class': ARTIST_SONGS_CLASS})
    # returns the artist, song name and href to the song lyrics page for each song
    return [{'name': song.text.strip(), 'href': song['href']} for song in songs_list]


def filter_song_lyrics(line):
    return line and not line.endswith('...')  # return true if the line isn't empty and not a repeated chorus


def extract_song_lyrics(song):
    soup = load_song_page(song['href'])
    name = soup.find('h1', {'class': SONG_NAME_CLASS})
    artist = soup.find('a', {'class': SONGS_ARTIST_CLASS})
    lyrics = soup.find('span', {'class': SONG_LYRICS_CLASS})
    return lyrics.text, name.text, artist.text


def get_quote():
    try:
        artist_id = ZOHAR_HAMELECH_ID
        song = random.choice(get_songs_list(artist_id))  # choose random song
        lyrics, song_name, artist = extract_song_lyrics(song)
        lines = list(filter(filter_song_lyrics, lyrics.splitlines()))  # filters unwanted lines
        line = random.choice(lines)  # choose random line
        return True, f'\"{line}\" - {song_name}, {artist}'
    except IndexError as e:
        return False, ''


def get_quotes(num=10):
    quotes = []
    pbar = tqdm(total=num)
    while len(quotes) < num:
        is_success, quote = get_quote()
        if is_success:
            quotes.append(quote)
            pbar.update(1)
    pbar.close()
    return quotes


def main():
    qs = get_quotes()
    for q in qs:
        print(q)


if __name__ == '__main__':
    main()
